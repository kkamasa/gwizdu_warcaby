/**
 * Created by krs on 28.01.16.
 */
var length = 5; //Length of the snake
var food = {};
var w = 450;
var h = 450;
var cw = 10;

var Game = function () {
    var that = this;

    this.minPlayersToStart = 2;
    this.loopRate = 60;

    this.addPoint = function (player) {
        that.score[player]++;
        console.log(that.score);
    };

    this.create =  function (session) {
        that.session = session;
        that.score = [0, 0];

        return this;
    };

    this.update = function (session) {
        //
    }
};


module.exports = Game;