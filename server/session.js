/**
 * Created by krs on 15.01.16.
 */
require('./helpers.js');
var game = require('./game.js');
var Client = require('./client.js');
var sessions = [];

function Session(io) {
    var that = this;
    this.id = sessions.length + 1 ;
    this.close = false;
    this.socktes = [];
    this.clients = [];
    this.game = new game();


    this.isOpen = function () {
        return !that.close;
    };

    this.joinSocket = function (client) {
        client.player = that.clients.length;

        client.join(that.id);

        that.clients.push(client);
        if (that.clients.length == that.game.minPlayersToStart) {
            that.start();
        }
        return that;
    };

    this.start = function () {
        console.log('session start id:', that.id);
        that.close = true;
        that.game.create(that);
        that.clients.forEach(function (c) {
            c.emit('start', {
                player: c.player,
            })
        });

        //Game loop
        that.gameLoopStart(that.game.loopRate);
    };

    this.gameLoopStart = function (rete) {
        console.log('game loop start', that.id)
        that.gameLoop = setInterval( function () {
            that.game.update(that);
        }, rete);
    };

    this.gameLoopStop = function () {
        console.log('game loop stop', that.id)
        clearInterval(that.gameLoop);
    };

    this.reset = function () {
        console.log('session reset', that.id);
        that.snakes = [];
        that.score = [0, 0];
        that.emitToAll('reset');
        that.game.create(that);
        that.gameLoopStart(that.game.loopRate);
    };

    this.emitToAll = function (name, data) {
        io.to(that.id).emit(name, data);
    };

    this.socketLeave = function (socket) {
        console.log('session close id:', that.id);
        that.clients.forEach(function (s , i) {
            if(s == socket){
                that.clients.splice(i)
            }else{
                s.emit('playerLeave')
            }
        });
        that.gameLoopStop();
    };

    this.gameOver = function () {
        var winnerIndex = that.game.score[0] > that.game.score[1]? 0 : 1;
        var msg = 'Gracz '+(winnerIndex+1)+" wygrywa!";

        if(that.game.score[0] == that.game.score[1]){
            msg = 'Remis';
        }

        that.gameLoopStop();
        that.emitToAll('gameOver', {
            msg: msg
        });

        setTimeout(that.reset, 3000);
    };

    console.log('session created id:',this.id);
    sessions.push(this);
}

Session.findOrCreate = function (io) {
    var session;
    if(session = sessions.find(function(s) {return s.isOpen();})) {
        return session;
    }else{
        return new Session(io);
    }
};

Session.find = function (id) {
    return sessions.find(function(s) {return s.id == id;});
};

Session.initClient = function (io, socket, done) {
    var client = socket;

    socket.on('ready', function () {
        socket.session = Session
            .findOrCreate(io)
            .joinSocket(socket);

        client = Client(socket);

        client.on('disconnect', function() {
            //console.log('Got disconnect!', socket.id);
            if( client.session){
                client.session.socketLeave(socket);
            }
        });

        done(client);
    });


};



module.exports = Session;