/**
 * Główny plik aplikacji
 *
 * Odpala server socket io na porcie podanym w .env
 * Inicuje router
 */

var app = require('http').createServer();
var env = require('node-env-file')('./.env');
var server = app.listen(env.PORT);

console.log('http server open, port:', env.PORT);

var io = require('socket.io')(server);

// init socket routes
require('./route.js')(io);
