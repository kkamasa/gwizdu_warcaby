/**
 * Created by krs on 01.02.16.
 */
var Session = require('./session.js');
var Client = require('./client.js');

//Game route
var route = function (io, client) {
    var session = client.session;
    var game = client.session.game;

    client.on('update', function (data) {
        client
            .getEnemy()
            .emit('updateDummy', data)
    });

    client.on('goal', function (data) {
        game.addPoint(data.player);
        session
            .emitToAll('releaseBall', {
                start: data.player,
                score: game.score
            }
        );
    });

};

module.exports = function(io){
    return io.on('connection', function (socket) {

        // init session client
        Session.initClient(io, socket, function (client) {
            // route init
            route(io, client);
        });

    })
};

