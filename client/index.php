<?php $serverAddress = 'http://' . $_SERVER['SERVER_NAME'] . ':9000' ?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Phaser - ping pong</title>
    <link rel="stylesheet" href="styles/style.css"/>
</head>
<body>
<div id="container">

</div>

<script src="<?= $serverAddress ?>/socket.io/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/phaser/2.4.4/phaser.js"></script>
<script type="text/javascript">
    var container = document.getElementById('container');
    var pingElement = document.getElementById("ping");

    var game = new Phaser.Game(500, 600, Phaser.AUTO, 'container', {preload: preload, create: create, update: update}),
        playerBet,
        player1BetInitPosY = 560,
        player2BetInitPosY = 40,
        dummyBet,
        dummyBetSpeed = 240,
        ball,
        infoText,
        masterText,
        scoreTexts = [],
        pingText,
        pingStartTime,
        ballSpeed = 300,
        ballReleased = false,
        isMaster = false;

    game.socket = io('<?=$serverAddress ?>');

    function releaseBall(data) {
        data = data ? data : {start: 1, score: [0, 1]};

        scoreTexts[0].text = data.score[0] || 0;
        scoreTexts[1].text = data.score[1] || 0;

        if (!isMaster) {
            return
        }
        ball.x = game.world.centerX;
        ball.y = game.world.centerY;
        if (!ballReleased) {
            ball.body.velocity.x = ballSpeed;
            ball.body.velocity.y = ballSpeed;

            console.log('velocity',ball.body.angle);
            ballReleased = true;
        }
    }

    function createBet(x, y) {
        var bet = game.add.sprite(x, y, 'bet');
        game.physics.arcade.enable(bet);

        bet.anchor.setTo(0.5, 0.5);
        bet.body.collideWorldBounds = true;
        bet.body.bounce.setTo(1, 1);
        bet.body.immovable = true;

        return bet;
    }

    function ballHitsBet(_ball, _bet) {
        var diff = 0;

        if (_ball.x < _bet.x) {
            //  ball on the left side of bet
            diff = _bet.x - _ball.x;
            _ball.body.velocity.x = ( -10 * diff );
        } else if (_ball.x > _bet.x) {
            //  ball on the right side of bet
            diff = _ball.x - _bet.x;
            _ball.body.velocity.x = ( 10 * diff );
        } else {
            //  ball on the center of bet. Adding some random velocity
            _ball.body.velocity.x = 2 + Math.random() * 8;
        }
    }

    function checkGoal() {
        if (!isMaster) {
            return
        }
        if (ball.y < player2BetInitPosY - 5) {
            setBall();
            game.socket.emit('goal', {player: 1})
        } else if (ball.y > player1BetInitPosY + 5) {
            setBall();
            game.socket.emit('goal', {player: 0})
        }
    }

    function setBall() {
        ballReleased = false;

        if (!isMaster) {
            ball.x = game.world.centerX;
            ball.y = game.world.centerY;
            ball.body.velocity.x = 0;
            ball.body.velocity.y = 0;
        }
    }

    function preload() {
        game.load.image('bet', 'assets/bet.png');
        game.load.image('ball', 'assets/ball.png');
        game.load.image('background', 'assets/bg.png');
    }

    function create() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.stage.disableVisibilityChange = true;
        game.add.tileSprite(0, 0, 500, 600, 'background');

        ball = game.add.sprite(game.world.centerX, game.world.centerY, 'ball');
        game.physics.arcade.enable(ball);

        ball.anchor.setTo(0.5, 0.5);
        ball.body.collideWorldBounds = true;
        ball.body.bounce.setTo(1, 1);

        var player1 = createBet(game.world.centerX, player1BetInitPosY);
        var player2 = createBet(game.world.centerX, player2BetInitPosY);

        masterText = game.add.text(25, 25, "");
        scoreTexts[0] = game.add.text(25, 50, "");
        scoreTexts[1] = game.add.text(25, 550, "");
        pingText = game.add.text(25, 100, "");
        infoText = game.add.text(game.world.centerX, game.world.centerY, "Czekaj na gracza", {
            font: "65px Arial",
            fill: "#ffff00",
            align: "center"
        });
        infoText.anchor.set(0.5);

        game.socket.emit('ready');

        game.socket.on('start', function (session) {
            console.log('session start');
            infoText.text = '';
            game.session = session;

            if (game.session.player > 0) {
                playerBet = player1;
                dummyBet = player2;
                isMaster = true;
//                masterText.text = 'Master'
            } else {
                playerBet = player2;
                dummyBet = player1;
//                masterText.text = 'Slave'
            }

            setInterval(sendUpdate, 2);

            game.socket.on('updateDummy', function (data) {
                dummyBet.x = data.x;
                if (!isMaster) {
                    ball.x = data.ball.x;
                    ball.y = data.ball.y;
                }
            });

            releaseBall();
        });


        game.socket.on('releaseBall', releaseBall);

    }

    function sendUpdate() {
        pingStartTime = Date.now();
        var data = {x: playerBet.x};
        if (isMaster) {
            data.ball = {x: ball.x, y: ball.y}
        }
        game.socket.emit('update', data);
    }

    function update() {
        pingElement.innerHTML = Date.now() - pingStartTime;
        if (!playerBet) {
            return
        }

        playerBet.x = game.input.x;

        var playerBetHalfWidth = playerBet.width / 2;

        if (playerBet.x < playerBetHalfWidth) {
            playerBet.x = playerBetHalfWidth;

        } else if (playerBet.x > game.width - playerBetHalfWidth) {
            playerBet.x = game.width - playerBetHalfWidth;
        }

        //Check collide ball and bets
        game.physics.arcade.collide(ball, playerBet, ballHitsBet, null, this);
        game.physics.arcade.collide(ball, dummyBet, ballHitsBet, null, this);

        checkGoal();
    }
</script>
</body>
</html>